import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './../interfaces/post';
import { Comment } from './../interfaces/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private PostApi = "https://jsonplaceholder.typicode.com/posts" ;
  private CommentApi = "https://jsonplaceholder.typicode.com/comments" ;

  userCollection:AngularFirestoreCollection = this.db.collection('Users');
  postCollection:AngularFirestoreCollection;
  
  constructor(private http: HttpClient ,private db: AngularFirestore) { }

// //For get posts from API
//   getPosts(): Observable<Post>
//  {
//     return this.http.get<Post>(`${this.PostApi}`);
//   }

//   getComments(): Observable<Comment>
//    {
//       return this.http.get<Comment>(`${this.CommentApi}`);
//     }

// //For Upload posts from API
    getPosts()
    {
        return this.http.get<Post[]>(`${this.PostApi}`);
    }

    getCollectionPost(userId):Observable<any[]>{
      this.postCollection = this.db.collection(`Users/${userId}/Posts`);
          console.log('private post collection created');
          return this.postCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
              const data = a.payload.doc.data();
              data.id = a.payload.doc.id;
              return  data ;
            }))
          ); 
    }

    getPost(userId, id:string):Observable<any>{
          return this.db.doc(`Users/${userId}/Posts/${id}`).get();
        }

    getComments()
    {
          return this.http.get<Comment[]>(`${this.CommentApi}`);
    }

    // //Add post function
    addPost(userId:string,title:string, body:string , ){
          const post = {title:title, body:body ,Like:0};
          this.userCollection.doc(userId).collection('Posts').add(post);
      }
// //Update like func
      UpdateLikes(userId:string, id:string,Like:number){
            this.db.doc(`Users/${userId}/Posts/${id}`).update(
               {
                 Like:Like
               }
             )
          }

       // Delete
    // Posts = the name of the Collection
    deletePost(userId:string , id:string){
      this.db.doc(`Users/${userId}/Posts/${id}`).delete();
    }


  

  }

