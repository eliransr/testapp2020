import { Component, OnInit } from '@angular/core';
import { PostsService } from './../../Services/posts.service';
import { AuthService } from './../../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  Posts$:Observable<any>;
  userId:string;
  public Like : number=0

 constructor(private postservie:PostsService, public auth:AuthService) { }

  ngOnInit() {
      //this.Posts$ = this.postservie.getPosts();
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
       this.Posts$ = this.postservie.getCollectionPost(this.userId);
         }
      )
  }

  addLike(id:string,Likes:number){
        this.Like = Likes + 1 ; 
        this.postservie.UpdateLikes(this.userId,id,this.Like)
     }

     deletePost(id){
      this.postservie.deletePost(this.userId,id)
      console.log(id);
     }

}