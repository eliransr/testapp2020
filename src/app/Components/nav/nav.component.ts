import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  title:string = "Home Page";
  email:string;
  userId:string


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    constructor(private breakpointObserver: BreakpointObserver,
      public auth:AuthService,
      location: Location, router: Router)
      {
        router.events.subscribe(val => {
          if (location.path() == "/welcom") {
            this.title = 'Home Page';
            console.log(val);        
          }
          else if (location.path() == "/blogposts" ){
            this.title = "Blog posts list";
          }

           else if (location.path() == "/savedposts" ){
            this.title = "Your saved posts list "  ;
          }
          else if (location.path() == "/login"){
            this.title = "Login form";
          }

          else if (location.path() == "/signup"){
                  this.title = "Sign up form";
                } 
           
        
        });   
      }

      ngOnInit() {
        this.title = this.auth.getMessage();
        this.email = this.auth.getMessage();
        this.auth.user.subscribe(
          user => {
            this.userId = user.uid;
            this.email = user.email;
           }
        )
    }
      
    }