import { PostsService } from './../../Services/posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from './../../interfaces/post';
import { Comment } from './../../interfaces/comment';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AuthService } from 'src/app/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  
    
      panelOpenState = false;
      // Posts$
      // Comments$

  // //For load to firebase
  Posts$: Post[];
  Comments$: Comment[];
  userId:string;
  message:string;
  postId:number;
  public Like : number=0
    
  constructor(private postservie: PostsService,private auth:AuthService) { }

     

      //function who upload in the database all the posts
  // saveFunc(){
  //     for (let index = 0; index < this.Posts$.length; index++) {
  //       for (let i = 0; i < this.Users$.length; i++) {
  //         if (this.Posts$[index].userId==this.Users$[i].id) {
  //           this.title = this.Posts$[index].title;
  //           this.body = this.Posts$[index].body;
  //           this.author = this.Users$[i].name;
  //           this.postsservice.addPosts(this.title , this.body, this.author);
            
  //         }
          
          
  //       }
        
  //     }
  //     this.message ="The data loading was successful"
  //   }
//function who upload in the database all the posts
savePost(title:string,body:string,id:number){
    this.postservie.addPost(this.userId,title,body)
    this.postId = id;
    this.message = "Saved for later viewing"
 }
    
      ngOnInit() {
        // this.Posts$ = this.postsservice.getPosts();
        // this.Comments$=this.postsservice.getComments();

        this.postservie.getPosts()
        .subscribe(data =>this.Posts$ = data );
        this.postservie.getComments()
        .subscribe(data =>this.Comments$ = data );
    
        this.auth.user.subscribe(
          user => {
            this.userId = user.uid;
           }
        )
      }

      onClick()
  {
    this.Like+=1;
  }
  savePostLike()
  {
    const counter =this.Like;
  }
    
    }