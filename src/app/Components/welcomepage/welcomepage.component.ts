import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-welcomepage',
  templateUrl: './welcomepage.component.html',
  styleUrls: ['./welcomepage.component.css']
})
export class WelcomepageComponent implements OnInit {

  title:string;
  email:string;
  userId:string
  constructor(public auth:AuthService) { }

  ngOnInit() {
    this.title = this.auth.getMessage();
    this.email = this.auth.getMessage();
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
       }
    )
}
  }


